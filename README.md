# (Fairy)Phy's osu! Beatmap Archive

これらは私が過去に作成したすべてのBeatmapアーカイブになります。これらはosuファイルのみ保管されているため、曲などはありません。

These will be all Beatmap archives I have created in the past. These are stored only osu files, so there are no songs etc.

## ライセンス / License

これらに特にライセンスはありません。ご自由に使ってもらえるとありがたいです。ただし、みんなが自由に使えるようにCC0として登録されています。

There is no specific license for these. You are free to use them as you wish. However, they are designated as CC0 so that everyone can use them freely.
